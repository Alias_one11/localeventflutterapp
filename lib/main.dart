import 'package:flutter/material.dart';
import './views/home_page/home_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  /// - PROPERTIES

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Material App',
      // Removes annoying debug banner!!!
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        scaffoldBackgroundColor: Color(0xFFFFFFFF),
        primaryColor: Color(0xFFFF4700)
      ),
      home: HomePage(),
      
    );
  }

  ///_>END - /* CLASS */
}

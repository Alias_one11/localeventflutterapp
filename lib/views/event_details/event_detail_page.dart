import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../models/events.dart';
import 'event_detail_background.dart';
import 'event_detail_content.dart';

class EventDetailPage extends StatelessWidget {
  /// - PROPERTIES
  final Event event;

  /// - /* CONSTRUCTOR */
  const EventDetailPage({
    Key key,
    this.event,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
	/// ZSTACK
	body: Provider<Event>.value(
	  value: event,
	  child: Stack(
		fit: StackFit.expand,
	    children: <Widget>[
	      EventDetailBackground(),
	  	EventDetailContent(),
	    ],
	  ),
	),
  );
}

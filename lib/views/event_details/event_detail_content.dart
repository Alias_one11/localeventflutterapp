import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import '../../models/guest.dart';
import '../../styleguide.dart';
import '../../models/events.dart';
import 'package:provider/provider.dart';

class EventDetailContent extends StatelessWidget {
  /// - GLOBAL-PROPERTIES

  @override
  Widget build(BuildContext context) {
    /// - LOCAL-PROPERTIES
    final Event event = Provider.of<Event>(context);
    final double screenWidth = MediaQuery.of(context).size.width;

    return SingleChildScrollView(
      /// VSTACK
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: 100),
          Padding(
            padding: EdgeInsets.symmetric(
              horizontal: screenWidth * 0.2,
            ),
            child: Text(
              event.title,
              style: eventWhiteTitleTextStyle,
            ),
          ),
          SizedBox(height: 10),
          Padding(
            padding: EdgeInsets.symmetric(
              horizontal: screenWidth * 0.22,
            ),
            child: FittedBox(
              child: Row(
                children: <Widget>[
                  Icon(
                    FontAwesome5Solid.location_arrow,
                    color: Colors.white,
                  ),
                  SizedBox(width: 10),
                  Text(
                    event.location,
                    style: eventLocationTextStyle.copyWith(
                      color: Colors.white,
                      fontWeight: FontWeight.w700,
                    ),
                  )
                ],
              ),
            ),
          ),
          SizedBox(height: 80),
          Padding(
            padding: const EdgeInsets.only(left: 16.0),
            child: Text(
              'GUEST',
              style: guestTextStyle,
            ),
          ),
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,

            /// HSTACK
            child: Row(
              children: <Widget>[
                /*@-----------------------------------------------------------@*/
                for (final Guest guest in guests)
                  //__________
                  /// Iterating through the guest images horizontally
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ClipOval(
                      child: Image.asset(
                        guest.imagePath,
                        width: 90,
                        height: 90,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                /*@-----------------------------------------------------------@*/
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.all(16.0),
            child: RichText(
              text: TextSpan(
                children: [
                  TextSpan(text: event.punchLine1, style: punchLine1TextStyle),
                  TextSpan(text: event.punchLine2, style: punchLine2TextStyle),
                ],
              ),
            ),
          ),
          /*@-----------------------------------------------------------@*/
          if (event.description.isNotEmpty)
            //__________
            /// If description is not empty
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Text(
                event.description,
                style: eventLocationTextStyle.copyWith(
                  fontWeight: FontWeight.bold,
                  letterSpacing: 2,
                ),
              ),
            ),
          /*@-----------------------------------------------------------@*/
          if (event.galleryImages.isNotEmpty ?? event.galleryImages)
            Padding(
              padding: const EdgeInsets.only(left: 16.0, top: 12.0),
              child: Text(
                'GALLERY',
                style: guestTextStyle,
              ),
            ),

          /// Bottom iterating images horizontally
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,

            /// HSTACK
            child: Row(
              children: <Widget>[
                /*@-----------------------------------------------------------@*/
                for (final gallery in event.galleryImages)
                  //__________
                  /// Iterating through the guest images horizontally
                  Container(
                    margin: const EdgeInsets.only(
                      left: 16.0,
                      right: 16.0,
                      bottom: 42.0,
                    ),
                    child: ClipRRect(
                      borderRadius: BorderRadius.all(
                        Radius.circular(20.0),
                      ),
                      child: Image.asset(
                        gallery,
                        width: 180,
                        height: 180,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                /*@-----------------------------------------------------------@*/
              ],
            ),
          ),
        ],
      ),
    );
  }
}

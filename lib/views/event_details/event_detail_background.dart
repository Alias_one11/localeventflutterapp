import 'package:flutter/material.dart';
import '../../models/events.dart';
import 'package:provider/provider.dart';

class EventDetailBackground extends StatelessWidget {
  /// - GLOBAL-PROPERTIES

  @override
  Widget build(BuildContext context) {
    /// - LOCAL-PROPERTIES
    final double screenWidth = MediaQuery.of(context).size.width;
    final double screenHeight = MediaQuery.of(context).size.height;
    final Event event = Provider.of<Event>(context);

    return Align(
      alignment: Alignment.topCenter,
      child: ClipPath(
        clipper: ImageClipper(),
        child: Image.asset(
          event.imagePath,
          fit: BoxFit.cover,
          width: screenWidth,
          color: Color(0x99000000),
          colorBlendMode: BlendMode.darken,
          height: screenHeight * 0.5,
        ),
      ),
    );
  }

  ///END--[ CLASS ]
}

///*-->@components <>CustomClipper*/
/// /*@-----------------------------------------------------------@*/
class ImageClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path();
    var curveStartingPoint = Offset(0, 40);
    var curveEndPoint = Offset(size.width, size.height * 0.95);
    path.lineTo(curveStartingPoint.dx, curveStartingPoint.dy - 5);

    path.quadraticBezierTo(
      size.width * 0.2,
      size.height * 0.85,
      curveEndPoint.dx - 60,
      curveEndPoint.dy + 10,
    );
    //__________
    path.quadraticBezierTo(
      size.width * 0.99,
      size.height * 0.99,
      curveEndPoint.dx,
      curveEndPoint.dy,
    );

    path.lineTo(size.width, 0);
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => true;
}

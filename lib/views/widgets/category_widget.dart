import 'package:flutter/material.dart';
import '../../appstate.dart';
import '../../styleguide.dart';
import 'package:provider/provider.dart';
import '../../models/categories.dart';

class CategoryWidget extends StatelessWidget {
  /// - PROPERTIES
  final Category category;

  const CategoryWidget({
    Key key,
    this.category,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    /// - PROPERTIES
    final AppState appState = Provider.of<AppState>(context);
    final bool isSelected = appState.selectedCategoryID == category.categoryId;

    return GestureDetector(
      onTap: () {
        /// Changes the state of the category color when the
        /// category is selected in the application
        if (!isSelected) {
          appState.updateCategoryID(category.categoryId);
        }
      },
      //__________
      child: Container(
        ///__{START}__
        //__________
        margin: const EdgeInsets.symmetric(horizontal: 8),
        width: 90,
        height: 90,
        // BoxDecoration
        decoration: BoxDecoration(
          border: Border.all(
            color: isSelected ? Colors.white : Color(0x99FFFFFF),
            width: 2,
          ),
          borderRadius: BorderRadius.all(
            Radius.circular(16),
          ),
          color: isSelected ? Colors.white : Colors.transparent,
        ),

        /// VSTACK
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            //__________
            /// ICONS
            Icon(
              category.icon,
              color: isSelected ? Theme.of(context).accentColor : Colors.white,
              size: 40,
            ),

            /// GAP BETWEEN
            SizedBox(height: 10),

            /// CATEGORY_NAME
            Text(
              category.name,
              style: isSelected ? selectedCategoryTextStyle : categoryTextStyle,
            )
          ],
        ),
        //__________
        ///__{END}__
      ),
    );
  }
}

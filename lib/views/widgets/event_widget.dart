import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:local_events_flutterapp/styleguide.dart';
import '../../models/events.dart';

class EventWidget extends StatelessWidget {
  /// - PROPERTIES
  final Event event;

  /// - /* CONSTRUCTOR */
  const EventWidget({
    Key key,
    this.event,
  }) : super(key: key);

  /*-->@override build<>METHOD*/
  @override
  Widget build(BuildContext context) => Card(
        /// CARD
        margin: const EdgeInsets.symmetric(vertical: 15.0),
        elevation: 4,
        color: Colors.white,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(24.0)),
        ),
        //__________
        child: Padding(
          padding: EdgeInsets.all(10.0),

          /// VSTACK
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              clipRRectImagePathComponent,

              /// HSTACK
              Padding(
                padding: const EdgeInsets.only(top: 8.0, left: 8.0),
                //__________
                child: Row(
                  children: <Widget>[
                    /// VSTACK
                    /// Event title & location
                    Expanded(
                        flex: 3, child: columnEventTitleAndLocationComponent),

                    /// Event duration
                    Expanded(flex: 1, child: textEventDurationComponent),
                  ],
                ),
              ),
            ],
          ),
        ),
      );

  ///*-->@components build<>METHODS*/
  /// /*@-----------------------------------------------------------@*/
  ClipRRect get clipRRectImagePathComponent => ClipRRect(
        borderRadius: BorderRadius.all(Radius.circular(30.0)),
        child: Image.asset(
          event.imagePath,
          height: 150.0,
          fit: BoxFit.fitWidth,
        ),
      );

/*@-----------------------------------------------------------@*/
  Column get columnEventTitleAndLocationComponent => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            event.title,
            style: eventTitleTextStyle,
          ),
          SizedBox(height: 10),

          /// HSTACK
          FittedBox(
            //__________
            child: Row(
              children: <Widget>[
                Icon(FontAwesome5Solid.location_arrow),
                SizedBox(width: 5),
                Text(
                  event.location,
                  style: eventLocationTextStyle,
                )
              ],
            ),
          ),
        ],
      );

/*@-----------------------------------------------------------@*/
  Text get textEventDurationComponent => Text(
        event.duration.toUpperCase(),
        textAlign: TextAlign.right,
        style: eventLocationTextStyle.copyWith(
          fontWeight: FontWeight.w900,
        ),
      );

  ///END--[ CLASS ]
}

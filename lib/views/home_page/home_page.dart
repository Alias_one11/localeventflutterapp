import 'package:flutter/material.dart';
import '../../views/event_details/event_detail_page.dart';
import '../../views/widgets/event_widget.dart';
import '../../models/events.dart';
import 'package:provider/provider.dart';
import '../../appstate.dart';
import '../../views/widgets/category_widget.dart';
import '../../models/categories.dart';
import '../../styleguide.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'home_page_background.dart';

class HomePage extends StatelessWidget {
  /// - PROPERTIES

  /*-->@override build<>METHOD*/
  @override
  Widget build(BuildContext context) => Scaffold(
        //__________
        /// ZSTACK
        body: ChangeNotifierProvider<AppState>(
          create: (_) => AppState(),

          /// ZSTACK<--PARENT WIDGET
          child: Stack(
            children: <Widget>[
              HomePageBackground(
                screenHeight: MediaQuery.of(context).size.height,
              ),
              //__________
              SingleChildScrollView(
                /// VSTACK
                child: Column(
                  /// .leading
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    // Helps with the cards not cutting off at the top of the screen
                    SizedBox(height: 56),

                    /// HSTACK
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 32.0),
                      //__________
                      child: Row(
                        children: <Widget>[
                          /* Local Events */
                          Text(
                            'Local Events',
                            style: fadedTextStyle,
                          ),
                          Spacer(),
                          /* Person Icon */
                          Icon(
                            EvaIcons.personAddOutline,
                            color: Color(0x99FFFFFF),
                            size: 30,
                          )
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 32.0),
                      child: Text(
                        'Que Lo Que!',
                        style: whiteHeadingTextStyle,
                      ),
                    ),

                    /// HSTACK scrolling horizontally
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 24.0),
                      child: Consumer<AppState>(
                        /*@-----------------------------------------------------------@*/
                        builder: (context, appstate, _) {
                          /// HSTACK
                          return scrollViewCategoryIteratorComponent;
                        },
                        /*@-----------------------------------------------------------@*/
                      ),
                    ),

                    /// Iterating through event titles when a category view is selected
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16.0),
                      child: Consumer<AppState>(
                        /*@-----------------------------------------------------------@*/
                        builder: (context, appstate, _) {
                          return titleIteratorComponent(
                            context: context,
                            appstate: appstate,
                          );
                        },
                        /*@-----------------------------------------------------------@*/
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      );

  ///*-->@components build<>METHODS*/
  /// /*@-----------------------------------------------------------@*/
  SingleChildScrollView get scrollViewCategoryIteratorComponent =>
      SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          children: <Widget>[
            /// Iterating through our category widget
            for (final category in categories)
              CategoryWidget(category: category),
          ],
        ),
      );

  /*@-----------------------------------------------------------@*/

  Column titleIteratorComponent({BuildContext context, AppState appstate}) =>
      Column(
        children: <Widget>[
          for (final event in events.where((e) {
            return e.categoryIds.contains(appstate.selectedCategoryID);
          }))
            GestureDetector(
              // Navigates to a particular event on tap of that view
              /*@-----------------------------------------------------------@*/
              onTap: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (_) => EventDetailPage(event: event)
                  ),
                );
              },
              /*@-----------------------------------------------------------@*/
              child: EventWidget(event: event),
            ),
        ],
      );

  ///END--[ CLASS ]
}

import 'package:flutter/material.dart';

class HomePageBackground extends StatelessWidget {
  /// - PROPERTIES
  final screenHeight;

  /// - /* CONSTRUCTOR */
  const HomePageBackground({
    Key key,
    @required this.screenHeight,
    //__________
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    /// - PROPERTIES
    final themeData = Theme.of(context);

    /*@-----------------------------------------------------------@*/
    return ClipPath(
      //__________
      clipper: BottomShapeClipper(),
      child: Container(
        height: screenHeight * 0.5,
        color: themeData.accentColor,
      ),
    );
  }
}

///*@-----------------------------------------------------------@*/
class BottomShapeClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path();
    Offset curveStartPoint = Offset(0, size.height * 0.85);
    Offset curveEndPoint = Offset(size.width, size.height * 0.85);

    path.lineTo(curveStartPoint.dx, curveStartPoint.dy);
    path.quadraticBezierTo(
      size.width / 2,
      size.height,
      curveEndPoint.dx,
      curveEndPoint.dy,
    );

    path.lineTo(size.width, 0);

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => true;
}

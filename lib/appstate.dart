import 'package:flutter/cupertino.dart';

/// ChangeNotifier: ↓↓↓
/// A class that can be extended or mixed in that provides a
/// change notification API using VoidCallback for notifications.
class AppState extends ChangeNotifier {
  /// - PROPERTIES
  var selectedCategoryID = 0;

  /* Class-methods */
  void updateCategoryID(int selectedCategoryID) {
    //__________
    this.selectedCategoryID = selectedCategoryID;

    /// Call this method whenever the object changes,
    /// to notify any clients the object may have changed.
    notifyListeners();
  }

  ///END--[ CLASS ]
}

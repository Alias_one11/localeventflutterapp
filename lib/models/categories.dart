///_>MODEL - /* Categories */
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';

class Category {
  final int categoryId;
  final String name;
  final IconData icon;

  Category({this.categoryId, this.name, this.icon});
}

final allCategory = Category(
  categoryId: 0,
  name: "All",
  icon: FontAwesome5Solid.search,
);

final musicCategory = Category(
  categoryId: 1,
  name: "Music",
  icon: FontAwesome5Solid.music,
);

final meetUpCategory = Category(
  categoryId: 2,
  name: "Meetup",
  icon: FontAwesome5Solid.location_arrow,
);

final basketballCategory = Category(
  categoryId: 3,
  name: "Basketball",
  icon: FontAwesome5Solid.basketball_ball,
);

final birthdayCategory = Category(
  categoryId: 4,
  name: "Birthday",
  icon: FontAwesome5Solid.birthday_cake,
);

final categories = [
  allCategory,
  musicCategory,
  meetUpCategory,
  basketballCategory,
  birthdayCategory,
];

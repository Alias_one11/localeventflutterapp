///_>MODEL - /* Events */

class Event {
  final String imagePath,
      title,
      description,
      location,
      duration,
      punchLine1,
      punchLine2;
  final List categoryIds, galleryImages;

  Event({
    this.imagePath,
    this.title,
    this.description,
    this.location,
    this.duration,
    this.punchLine1,
    this.punchLine2,
    this.categoryIds,
    this.galleryImages,
  });
}

final fiveKmRunEvent = Event(
  imagePath: "assets/event_images/5_km_downtown_run.jpeg",
  title: "5 Kilometer Downtown Run",
  description: "Running cause we can!",
  location: "Pleasant Park",
  duration: "3h",
  punchLine1: "Marathon!",
  punchLine2: "The latest fad in foodology, get the inside scoup.",
  galleryImages: [],
  categoryIds: [0, 1],
);

final cookingEvent = Event(
  imagePath: "assets/event_images/granite_cooking_class.jpeg",
  title: "Granite Cooking Class",
  description:
      "Guest list fill up fast so be sure to apply before hand to secure a spot.",
  location: "Food Court Avenue",
  duration: "4h",
  punchLine1: "Granite Cooking",
  punchLine2: "The latest fad in foodology, get the inside scoop.",
  categoryIds: [0, 2],
  galleryImages: [
    "assets/event_images/cooking_1.jpeg",
    "assets/event_images/cooking_2.jpeg",
    "assets/event_images/cooking_3.jpeg"
  ],
);

final musicConcert = Event(
  imagePath: "assets/event_images/music_concert.jpeg",
  title: "SXSW Music Concert",
  description: "Corona-Virus Free Concert.",
  location: "Austin, Texas",
  duration: "1-Week",
  punchLine1: "Enjoy The Music!",
  punchLine2: "The latest fad in foodology, get the inside scoop.",
  galleryImages: [
    "assets/event_images/cooking_1.jpeg",
    "assets/event_images/cooking_2.jpeg",
    "assets/event_images/cooking_3.jpeg"
  ],
  //__________
  categoryIds: [0, 1],
);

final basketballCompetition = Event(
  imagePath: "assets/event_images/basketball_event.jpg",
  title: "Corona Ball Up Challenge",
  description: "Competitive basketball tournament",
  location: "Free Corona-Park, Pluto",
  duration: "1d",
  punchLine1: "Check Ball!",
  punchLine2: "The latest fad in foodology, get the inside scoop.",
  galleryImages: [
    "assets/event_images/cooking_1.jpeg",
    "assets/event_images/cooking_2.jpeg",
    "assets/event_images/cooking_3.jpeg"
  ],
  //__________
  categoryIds: [0, 3],
);

final events = [
  fiveKmRunEvent,
  cookingEvent,
  musicConcert,
  basketballCompetition,
];
